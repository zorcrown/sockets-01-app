var socket = io();
//escuchar informacion
socket.on('connect', function() {
    console.log('conectado al servidor');
});

socket.on('disconnect', function() {
    console.log('perdimos coneccion con el servidor');
});
//enviar informacion 
socket.emit('enviarMensaje', {
    usuario: 'Cristian',
    mensaje: 'Hola mundo'
}, function(resp) {
    console.log('Mensaje de server', resp);
});

//escuchar informacion 

socket.on('enviarMensaje', function(mensaje) {
    console.log('servidor', mensaje);
});